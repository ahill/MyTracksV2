@echo off
TITLE Update Latex Documentation

REM refference http://www.robvanderwoude.com/batchcommands.php

echo Running latex...
call ./latex/make.bat REM > NUL 2>&1
Echo Done...
echo Copying the new documentation file to MyTracks_Technical_Documentation.pdf
COPY ".\latex\refman.pdf" ".\MyTracks_Technical_Documentation.pdf" > NUL
pause