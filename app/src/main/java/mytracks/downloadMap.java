package mytracks;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by ahill on 4/8/2015.
 */


// goal: download the needed map from the primary server.
//

//! Provides a method of downloading a map from a remote server.
//!
//! This class is asyncronus and so it will return before the download is complete.
//! 
//! @ToDo impliment a call back that allows the calling object to receive a signel when the download is complete.
//! 
public class downloadMap {
    String server = "https://mytracks.labhill.com/maps/";
    static boolean Done;

    downloadMap(String MapName){
        Done = false;
        Log.d("downloadMap"," Started...");
        new HttpAsyncTask().execute(server + MapName);
        Log.d("downloadMap"," Finished");

    }

    //! returns the state of the download.
    //! @returns true if the download is done and false otherwise.
    public boolean isDone(){
        return Done;
    }

    //! downloads the indicated url to 
    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();
            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
            // convert inputstream to string
            if(inputStream != null){
                File targetFile = new File(Environment.getExternalStorageDirectory() + "/new-jersey.map");
                Log.d("downloadMap"," Downloading to " + Environment.getExternalStorageDirectory() + "/new-jersey.map");
                FileUtils.copyInputStreamToFile(inputStream, targetFile);
                Done = true;
                //result = convertInputStreamToString(inputStream);
            }else{
                Log.d("downloadMap", "File download failed:(");
            }

        } catch (Exception e) {
            Log.d("downloadMap", e.getLocalizedMessage());
        }
        return result;
    }


    //! an async class that will request the url
    //!
    //! @ToDo add onPostExecute: this should contact the caller and alert
    //!          them that the download is complete.
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(getBaseContext(), "Received!", Toast.LENGTH_LONG).show();
            //mTextView.setText(result);
        }
    }

    // convert inputstream to String
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}











