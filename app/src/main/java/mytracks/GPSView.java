package mytracks;

/**
 * Created by Chris Geraldpaulraj on 3/18/2015. Not fully implemented Test1
 */
public class GPSView {
    public int LatitudetoY(double Latitude, int mapWidth, int mapHeight)
    {
        double minLat = -85.05112878;
        double maxLat = 85.05112878;
        double yScale = mapHeight / (maxLat - minLat);
        double  y2 = - (Latitude + minLat) * yScale;
        int y = (int)y2;
        return y;
    }

    public int LongitudetoX(double Longitude, int mapWidth, int mapHeight)
    {
        double minLong = -180;
        double maxLong = 180;
        double xScale = mapWidth/ (maxLong - minLong);
        double x2 = (Longitude - minLong) * xScale;
        int x = (int)x2;
        return x;
    }

    public int CenterLat(double Latitude, int mapWidth, int mapHeight)
    {
        return 0;
    }

    public int CenterLong(double Longitude, int mapWidth, int mapHeight)
    {
        return 0;
    }

 /* This gets the center point in java
 private void formWindowActivated(java.awt.event.WindowEvent evt)
{
        // Get the size of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        // Determine the new location of the window
        int w = this.getSize().width;
        int h = this.getSize().height;
        int x = (dim.width-w)/2;
        int y = (dim.height-h)/2;

        // Move the window
        this.setLocation(x, y);
}
  */


}
