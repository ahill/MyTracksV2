package mytracks;

import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.common.api.GoogleApiClient;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

//import mytracks.download.TracksViewActivity;

import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import org.mapsforge.core.graphics.Paint;
import org.mapsforge.core.graphics.Style;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;

import org.mapsforge.core.model.LatLong;
import org.mapsforge.map.android.util.AndroidUtil;
//import org.mapsforge.map.android.view.MapView;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.overlay.FixedPixelCircle;
import org.mapsforge.map.layer.overlay.Polyline;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.reader.MapDataStore;
import org.mapsforge.map.reader.MapFile;
import org.mapsforge.map.rendertheme.InternalRenderTheme;



public class MainActivity extends Activity implements ViewAdapter.OnItemClickListener {

    double lat;
    double lon;
    List<LatLong> pathCoords;
    Polyline Path;

    Paint uLocPaint;
    FixedPixelCircle UserLoc;


    GPSTracker gps;

    private MytracksMapView mapView;
    private TileCache tileCache;
    private TileRendererLayer tileRendererLayer;

    List<Track> localTracks = new ArrayList<Track>();
    boolean RecordTrack = false;

    private DrawerLayout mDrawerLayout;
    private RecyclerView mDrawerList;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] screens = {"Download Maps", "View Tracks", "Settings"};


    Polyline localpath;
    List<LatLong> localPathCoords;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Initialize mapsforge//
        AndroidGraphicFactory.createInstance(this.getApplication());

        setContentView(R.layout.activity_main);

//<<<<<<< HEAD
        //mapsforge initialize
//        this.mapView = new MapView(this);


//<<<<<<< HEAD
//        Button record = new Button(this);
//        record.setText("Record");
//        Button stopRecord = new Button(this);
//        stopRecord.setText("Stop");
//
//        Drawable d = getResources().getDrawable(R.drawable.ic_my_location_black_36dp);
//        Button center = new Button(this);
//        center.setBackground(d);
//
//        LinearLayout linearLayout = new LinearLayout(this);
//        linearLayout.setOrientation(LinearLayout.VERTICAL);
//        LinearLayout.LayoutParams llparams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT);
//
//        linearLayout.addView(record, llparams);
//        linearLayout.addView(stopRecord, llparams);
//
//        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.WRAP_CONTENT,
//                RelativeLayout.LayoutParams.WRAP_CONTENT);
//        p.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//        p.addRule(RelativeLayout.CENTER_VERTICAL);
//        p.setMarginEnd(40);
//
//        RelativeLayout.LayoutParams a = new RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.WRAP_CONTENT,
//                RelativeLayout.LayoutParams.WRAP_CONTENT);
//        a.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//        a.addRule(RelativeLayout.ALIGN_PARENT_END);
//        a.setMarginEnd(90);
//        a.setMargins(0,0,0,200);
//
//        RelativeLayout relativeLayout = new RelativeLayout(this);
//        relativeLayout.addView(this.mapView);
//        relativeLayout.addView(linearLayout, p);
//        relativeLayout.addView(center, a);
//
//        RelativeLayout.LayoutParams lParams = new RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.MATCH_PARENT,
//                RelativeLayout.LayoutParams.MATCH_PARENT);
//
//        //   setContentView(this.mapView);
//        setContentView(relativeLayout, lParams);

//        RelativeLayout rlMap = (RelativeLayout) findViewById(R.id.rlMap);
//        rlMap.addView(this.mapView,0);
//=======
//        Button record = new Button(this);
//        record.setText("Record");
//        Button stopRecord = new Button(this);
//        stopRecord.setText("Stop");
//>>>>>>> 27dc0527afafedcaa2d02cf63944d4d8a1309685
//=======

        ////////////////////////////////////////////////////////////////////////////////////////////
        //// Side Drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (RecyclerView) findViewById(R.id.left_drawer);
        mDrawerList.setHasFixedSize(true);

        mDrawerList.setLayoutManager(new LinearLayoutManager(this));
//>>>>>>> d4bfa86373594a8c6bd0d92e51c98655e8f83798

        mDrawerList.setAdapter(new ViewAdapter(screens, this));

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        mDrawerList.addItemDecoration(itemDecoration);


        ////////////////////////////////////////////////////////////////////////////////////////////
        //// MapsForge
        this.mapView = new MytracksMapView(this);
        mapView.setCenterMap(true);  // by default center the map
        RelativeLayout rlMap = (RelativeLayout) findViewById(R.id.rlMap);
        rlMap.addView(this.mapView,0);

        this.mapView.setClickable(true);
        this.mapView.getMapScaleBar().setVisible(true);
        this.mapView.setBuiltInZoomControls(true);
        this.mapView.getMapZoomControls().setZoomLevelMin((byte) 10);
        this.mapView.getMapZoomControls().setZoomLevelMax((byte) 20);
        tileCache = AndroidUtil.createTileCache(this,"mapcache",mapView.getModel().displayModel.getTileSize(), 1f, this.mapView.getModel().frameBufferModel.getOverdrawFactor());


        ////////////////////////////////////////////////////////////////////////////////////////////
        //// Overlay Buttons
        final ImageButton record = (ImageButton) findViewById(R.id.record);
        ImageButton center = (ImageButton) findViewById(R.id.center);
        ImageButton menu = (ImageButton) findViewById(R.id.menu);
        record.setColorFilter(0xff626262);
        center.setColorFilter(0xff626262);


        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Record", Toast.LENGTH_SHORT).show();
                if (!RecordTrack) {
                    // start recording
                    record.setColorFilter(0xff2E7D32);
                    RecordTrack = true;
                    Track t = new Track(getApplicationContext(), "Track " + localTracks.size());

                    localTracks.add(t);

                } else {
                    record.setColorFilter(0xff626262);
                    RecordTrack = false;

                }
            }
        });


        center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Center on current location",Toast.LENGTH_SHORT).show();
                //centerMap = true;
                mapView.setCenterMap(true);
                mapView.getModel().mapViewPosition.setCenter(new LatLong(lat,lon));
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.START);
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////
        //  Initialize Tracer
        final Paint pathdraw = AndroidGraphicFactory.INSTANCE.createPaint();
        pathdraw.setColor(Color.BLUE);
        pathdraw.setStrokeWidth(45);
        pathdraw.setStyle(Style.STROKE);
        // Initializer paint for the user's current location
        uLocPaint = AndroidGraphicFactory.INSTANCE.createPaint();
        uLocPaint.setColor(Color.RED);

        UserLoc = new FixedPixelCircle(new LatLong(lat,lon), 6, uLocPaint, uLocPaint);




        //Initialize polyline
        Path = new Polyline(pathdraw, AndroidGraphicFactory.INSTANCE);
        //Initialize draw coords
        pathCoords = Path.getLatLongs();

        //////////////////////////////////////////////////////////
        // Initialize the GPS
        gps = new GPSTracker(this);
        if(gps.canGetLocation()){
            Toast.makeText(getApplicationContext(),"GPS is Working :)",Toast.LENGTH_SHORT).show();
        }

//<<<<<<< HEAD


        gps.createLocationRequest();
        gps.Gclient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(gps)
                .addOnConnectionFailedListener(gps)
                .build();




        //final customDraw dr = (customDraw) this.findViewById(R.id.view);
        //final customDraw dr = null;

       // final Handler handler = new Handler();
//=======
//>>>>>>> d4bfa86373594a8c6bd0d92e51c98655e8f83798


        ////////////////////////////////////////////////////////////////////////////////////////////
        // create an asynchronous task to update the GPS position every 1 second
        final Handler handler = new Handler();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                //Perform background work here
                //Toast.makeText(getApplicationContext(),"Moving the drawing...",Toast.LENGTH_SHORT).show();
                handler.post(new Runnable() {
                    public void run() {

                        double mySpeed = gps.getSpeed() * 2.23694;  // convert from m/s to MPH

                        if(gps.getUpdates() != 0)
                        {
                            lat = gps.getLatitude();
                            lon = gps.getLongitude();
                            if(mapView.isCenterMap())
                                mapView.getModel().mapViewPosition.setCenter(new LatLong(lat,lon));




                            if(RecordTrack){
                                if(gps.getAccuracy() < 30.0) {
                                    pathCoords.add(new LatLong(lat, lon));

                                    if (mapView.getLayerManager().getLayers().isEmpty()) {
                                        mapView.getLayerManager().getLayers().add(Path);
                                        WriteCoordsToText(false,lat,lon);
                                    } else {
                                        mapView.getLayerManager().getLayers().remove(Path);
                                        mapView.getLayerManager().getLayers().add(Path);
                                        WriteCoordsToText(false,lat,lon);
                                    }
                                    if(localTracks.size() > 0){
                                        localTracks.get(localTracks.size()-1).AddPoint(lat,lon);
                                    }
                                }

                            }else{
                                if(!pathCoords.isEmpty())
                                {
                                    Path = new Polyline(pathdraw, AndroidGraphicFactory.INSTANCE);
                                    pathCoords = Path.getLatLongs();
                                    WriteCoordsToText(true,1,1);
                                }
                            }


                            if(mapView.getLayerManager().getLayers().isEmpty())
                            {
                                mapView.getLayerManager().getLayers().add(UserLoc);
                            }
                            else
                            {
                                mapView.getLayerManager().getLayers().remove(UserLoc);
                                UserLoc = new FixedPixelCircle(new LatLong(lat,lon), 4, uLocPaint, uLocPaint);
                                mapView.getLayerManager().getLayers().add(UserLoc);
                            }



                        }

                     }
                });
            }
        };
        Timer timer = new Timer();
        timer.schedule( doAsynchronousTask, 10, 1000);

    }



    @Override
    protected void onStart(){
        super.onStart();


        initilizeMapsForge();


        LoadSavedCoords();

    }

    // attempts to initialize the MapsForge display
    // if the device contains the needed map then MapsForge is initilized imadatly
    //  otherwise the map file is downloaded from a remote server.
    private void initilizeMapsForge(){
        this.mapView.getModel().mapViewPosition.setCenter(new LatLong(40.0583238, -74.4056612));
        this.mapView.getModel().mapViewPosition.setZoomLevel((byte) 12);

        String mapFile = Environment.getExternalStorageDirectory() + "/new-jersey.map";
        File file = new File(mapFile);

        if(file.exists()) {
            //Log.d("initilizeMapsForge"," File " + Environment.getExternalStorageDirectory() + "/new-jersey.map exists");
            MapDataStore mapDataStore = new MapFile(file);
            System.out.println("Map file location: " + Environment.getExternalStorageDirectory());
            this.tileRendererLayer = new TileRendererLayer(tileCache, mapDataStore, this.mapView.getModel().mapViewPosition, false, true, AndroidGraphicFactory.INSTANCE);
            tileRendererLayer.setXmlRenderTheme(InternalRenderTheme.OSMARENDER);
            this.mapView.getLayerManager().getLayers().add(tileRendererLayer);
        }else{
            // download the file first
            //Log.d("initilizeMapsForge"," NO File " + Environment.getExternalStorageDirectory() + "/new-jersey.map");
            downloadMap d = new downloadMap("new-jersey.map");

            file = new File(Environment.getExternalStorageDirectory(), "new-jersey.map");
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (RecordTrack){
            ImageButton record = (ImageButton) findViewById(R.id.record);
            record.setColorFilter(0xff2E7D32);
        }
    }

    @Override
    protected void onStop(){
        super.onStop();
        this.mapView.getLayerManager().getLayers().remove(this.tileRendererLayer);
        this.tileRendererLayer.onDestroy();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        this.tileCache.destroy();
        this.mapView.getModel().mapViewPosition.destroy();
        this.mapView.destroy();
        AndroidGraphicFactory.clearResourceFileCache();
    }

    @Override
    public void onClick(View view, int position) {
        selectItem(position);
    }

    private void selectItem(int position) {

        if (position == 0) {    // Download Maps selected
            Intent intent = new Intent(this, DownloadActivity.class);
            startActivity(intent);
        }else if(position == 1){ // View Tracks selected

            Intent intent = new Intent(this, ViewTracksActivity.class);
            startActivity(intent);

        }
    }



    public void WriteCoordsToText(boolean isDivider, double lat, double lon)
    {
        File CoordDump;
        try {
            CoordDump = new File(Environment.getExternalStorageDirectory(), "MyTracks");
            if (!CoordDump.exists()) {
                CoordDump.mkdirs();
            }

            File TxtFile = new File(CoordDump, "SavedCoords.txt");
            FileWriter Writer;
            if(TxtFile.exists()) {
                Writer = new FileWriter(TxtFile, true);
            }
            else
            {
                Writer = new FileWriter(TxtFile);
            }


            if (isDivider) {
                Writer.append("#\n");
            } else {
                Writer.append(Double.toString(lat) + "," + Double.toString(lon) + "\n");
            }

            Writer.flush();
            Writer.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }



    public void LoadSavedCoords()
    {
        try {
            double latitude;
            double longitude;
            File fileloc = new File(Environment.getExternalStorageDirectory(), "MyTracks");
            File file = new File(fileloc, "SavedCoords.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            StringTokenizer tokens;

            final Paint LocalPathsPaint = AndroidGraphicFactory.INSTANCE.createPaint();
            LocalPathsPaint.setColor(Color.GREEN);
            LocalPathsPaint.setStrokeWidth(45);
            LocalPathsPaint.setStyle(Style.STROKE);


            localpath = new Polyline(LocalPathsPaint, AndroidGraphicFactory.INSTANCE);
            localPathCoords = localpath.getLatLongs();


            while((line = br.readLine()) != null){
                if(line.contains("#"))
                {
                    localpath = new Polyline(LocalPathsPaint, AndroidGraphicFactory.INSTANCE);
                    localPathCoords = localpath.getLatLongs();
                }
                else
                {
                    tokens = new StringTokenizer(line, ",");
                    latitude = Double.parseDouble(tokens.nextToken());
                    longitude = Double.parseDouble(tokens.nextToken());


                    localPathCoords.add(new LatLong(latitude, longitude));

                    if (mapView.getLayerManager().getLayers().isEmpty()) {
                        mapView.getLayerManager().getLayers().add(localpath);
                    } else {
                        mapView.getLayerManager().getLayers().remove(localpath);
                        mapView.getLayerManager().getLayers().add(localpath);
                    }
                }
            }


        }catch(IOException e)
        {
            Toast.makeText(getApplicationContext(), "Saved file not found", Toast.LENGTH_SHORT).show();

        }
    }

}
