package mytracks;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dave on 4/13/2015.
 */
public class DownloadAdapter extends RecyclerView.Adapter<DownloadAdapter.ViewHolder> {
    private ArrayList<DownloadActivity.mapData> data = new ArrayList<DownloadActivity.mapData>();
    private ArrayList<String> mapSize = new ArrayList<String>();

    private OnItemClickListener mListener;
    private SparseBooleanArray selectedItems;

    public DownloadAdapter(ArrayList<DownloadActivity.mapData> a, OnItemClickListener listener) {
//        mapName = data;
        data = a;
        mListener = listener;
        selectedItems = new SparseBooleanArray();
    }

    public void addData(DownloadActivity.mapData item, int position) {
        data.add(position, item);
        notifyItemInserted(position);
    }

    public interface OnItemClickListener {
        public void onClick(View view, int position, DownloadActivity.mapData item);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mapstringTextView;
        public final TextView sizestringTextView;
        public final TextView datestringTextView;
        public final LinearLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            mapstringTextView = (TextView) itemView.findViewById(R.id.txt_label_item);
            sizestringTextView = (TextView) itemView.findViewById(R.id.txt_size);
            datestringTextView = (TextView) itemView.findViewById(R.id.txt_date);
            layout = (LinearLayout) itemView.findViewById(R.id.container_inner_item);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vi = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.download_row, parent, false);
        ViewHolder vh = new ViewHolder(vi);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (data.get(position).getDate().equals("")) {
            holder.mapstringTextView.setText(data.get(position).getName());
            holder.sizestringTextView.setText(data.get(position).getSize() + "          " +
                    "                                        " + "\u25BC");
            holder.datestringTextView.setText("");
            holder.mapstringTextView.setTextColor(0xffffffff);
            holder.sizestringTextView.setTextColor(0xB3ffffff);
        }
        else {
            holder.mapstringTextView.setText("  " + data.get(position).getName());
            holder.sizestringTextView.setText("   " + data.get(position).getSize());
            holder.datestringTextView.setText(data.get(position).getDate());

            holder.mapstringTextView.setTextColor(0xB3ffffff);
            holder.sizestringTextView.setTextColor(0x80ffffff);
            holder.datestringTextView.setTextColor(0x40ffffff);

        }
        holder.itemView.setActivated(selectedItems.get(position, false));

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onClick(view, position, data.get(position));
            }
        });
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        }
        else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}


