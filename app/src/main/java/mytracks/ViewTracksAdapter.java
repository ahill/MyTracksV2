package mytracks;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ViewTracksAdapter extends RecyclerView.Adapter<ViewTracksAdapter.ViewHolder> {
    private List<String> items = new ArrayList<String>();

    private int itemLayout;
    private Context parrentContext;

    public ViewTracksAdapter(List<String> items,Context c) {
        parrentContext = c;
        this.items = items;
        ArrayList<File> files = new ArrayList<File>();
        listf(parrentContext.getFilesDir().getAbsolutePath(),files);
        for(int w = 0; w < files.size(); w++){
            items.add(files.get(w).getName());
        }




        this.itemLayout = itemLayout;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        View vi = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);
        ViewHolder vh = new ViewHolder(vi);
        return vh;
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        String item = items.get(position);
        holder.itemName.setText("Item " + position);
        holder.itemSize.setText(position + " Gb");

    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView icon;
        public final TextView itemName;
        public final TextView itemSize;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            itemName = (TextView) itemView.findViewById(R.id.track_name);
            itemSize = (TextView) itemView.findViewById(R.id.txt_size);
        }
    }

    public void listf(String directoryName, ArrayList<File> files) {
        File directory = new File(directoryName);

// get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile()) {
                files.add(file);
            } //else if (file.isDirectory()) {
              //  listf(file.getAbsolutePath(), files);
            //}
        }
    }
}