package mytracks;

import android.content.Context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.cert.PolicyNode;
import java.util.ArrayList;

/**
 * Created by Alexander on 3/18/2015.
 */



public class Track {
    // point count
    // point type
    // date
    // time
    // user
    pathType trackType;
    String Name;
    File dataFile;
    Context parrentContxt;

    FileOutputStream stream;
    Track(Context parContext,String s){
        parrentContxt = parContext;
        Name = s;
        System.out.println("Creating file: " + Name + ".trk");
        dataFile = new File(parContext.getFilesDir(), s + ".trk");
        //if(dataFile.)){
            try {
                stream = new FileOutputStream(dataFile);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String tmp = "Name:" + Name + "\r\n";
            try {
                stream.write(tmp.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }

        //}
    }

    public ArrayList<Point> Points = new ArrayList<Point>();

    void AddPoint(double Lat, double Lon) {
        System.out.println("Adding point (" + Lat + ", " + Lon + " to " + Name);
        Points.add(new Point(Lat, Lon));
        String data = Lat + ":" + Lon + "\n";
        try {

            stream.write(data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void setName(String s){
        Name = s;
    }

    String getName(){
        return Name;
    }

    Point getPoint(int index){
        return Points.get(index);
    }

    int getPintCount(){
        return Points.size();
    }

    pathType getType(){
        return trackType;
    }

    void setType(pathType Type){
        trackType = Type;
    }
}



