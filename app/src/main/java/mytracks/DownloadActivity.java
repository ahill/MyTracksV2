package mytracks;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toolbar;

import mytracks.R;



public class DownloadActivity extends Activity implements DownloadAdapter.OnItemClickListener, ActionMode.Callback {
    private RecyclerView mRecyclerView;
    private DownloadAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public String[] places = {"US", "Canada", "Asia", "Europe", "South America"};

    private ArrayList<mapData> data = new ArrayList<mapData>();
    private ArrayList<mapData> US = new ArrayList<mapData>();
    private ArrayList<mapData> Canada = new ArrayList<mapData>();
    private ArrayList<mapData> Europe = new ArrayList<mapData>();
    private ArrayList<mapData> South_America = new ArrayList<mapData>();
    private ArrayList<mapData> Asia = new ArrayList<mapData>();
    private ArrayList<mapData> Misc = new ArrayList<mapData>();

    ProgressDialog mProgressDialog;
    ActionMode actionMode;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        for(int i = 0; i<places.length; i++){
            mapData a = new mapData();
            a.setName(places[i]);
            data.add(a);
        }

        new Title().execute();



        mAdapter = new DownloadAdapter(data, this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());


        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        mRecyclerView.addItemDecoration(itemDecoration);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_download, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_download) {
            return true;
        }

        if (id == R.id.action_delete) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view, int position, DownloadActivity.mapData item) {
        Log.d("TAGGGGG", item.getName() + "    " + item.getUrl());
        switch (item.getName()){
            case "US":
                expand(item, US, position);
                break;
            case "Canada":
                expand(item, Canada, position);
                break;
            case "Asia":
                expand(item, Asia, position);
                break;
            case "Europe":
                expand(item, Europe, position);
                break;
            case "South America":
                expand(item, South_America, position);
                break;
        }
        mAdapter.toggleSelection(position);
    }

    void expand(DownloadActivity.mapData item, ArrayList<mapData> a, int position){
        if (!item.isExpanded()) {
            data.addAll(position + 1, a);
            mAdapter.notifyItemRangeInserted(position + 1, a.size());
            item.toggleExpand();
        }
        else{
            data.removeAll(a);
            item.toggleExpand();
            mAdapter.notifyItemRangeRemoved(position + 1, a.size());
        }
    }

    private void selectItem(int position) {
        mAdapter.notifyItemInserted(position+1);

    }



    private class Title extends AsyncTask<Void, Void, Void> {
        String title;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(DownloadActivity.this);
            mProgressDialog.setTitle("Map Data");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            fillDataStructure("http://ftp-stud.hs-esslingen.de/pub/Mirrors/download.mapsforge.org/maps/north-america/us/", US);
            fillDataStructure("http://ftp-stud.hs-esslingen.de/pub/Mirrors/download.mapsforge.org/maps/north-america/canada/", Canada);
            fillDataStructure("http://ftp-stud.hs-esslingen.de/pub/Mirrors/download.mapsforge.org/maps/asia/", Asia);
            fillDataStructure("http://ftp-stud.hs-esslingen.de/pub/Mirrors/download.mapsforge.org/maps/europe/", Europe);
            fillDataStructure("http://ftp-stud.hs-esslingen.de/pub/Mirrors/download.mapsforge.org/maps/south-america/", South_America);
            fillDataStructure("http://ftp-stud.hs-esslingen.de/pub/Mirrors/download.mapsforge.org/maps/", Misc);

            return null;
        }

        public ArrayList<mapData> fillDataStructure(String aURL, ArrayList<mapData> structure){

            try {
                Document document = Jsoup.connect(aURL).get();
                Elements imports = document.select("a[href$=.map]");
                Elements size = document.select("[attr*=M|G]");
                for (Element link : imports) {
                    mapData a = new mapData();
                    a.setUrl(link.attr("abs:href"));
                    structure.add(a);                }
                int i = 0;
                for (Element table : document.select("table")) {
                    for (Element row : table.select("tr")) {
                        Elements tds = row.select("td");
                        if (tds.size() > 1) {
                            Log.d("TAGGGGG", tds.get(0).text() + tds.get(1).text() + "   " + tds.get(2).text() + tds.get(3).text());
                            String r = tds.get(3).text();
                            String t = tds.get(1).text();
                            String d = tds.get(2).text();
                            if ((r.contains("G") || r.contains("M") || r.contains("K")) && (t.contains(".map"))) {
                                structure.get(i).setSize(r + "B");
                                structure.get(i).setName(t.replace(".map", ""));
                                structure.get(i).setDate("Last modified: " + d.substring(0, d.length() - 5));
                                i++;
                            }

                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return structure;
        }

        public double getSizeSum(ArrayList<mapData> structure){
            double sum = 0;
            String a;
            for (int i = 0; i<structure.size(); i++){
                a = structure.get(i).getSize();
                a = a.replace("G", "");
                a = a.replace("M", "");
                a = a.replace("K", "");
                a = a.replace("B", "");
                sum = sum + Double.parseDouble(a);
            }
            return sum;
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgressDialog.dismiss();
            data.get(0).setSize(Integer.toString((int) getSizeSum(US)) + "MB");
            data.get(1).setSize(Integer.toString((int) getSizeSum(Canada)) + "MB");
            data.get(2).setSize(Integer.toString((int) getSizeSum(Asia)) + "MB");
            data.get(3).setSize(Integer.toString((int) getSizeSum(Europe)) + "MB");
            data.get(4).setSize(Integer.toString((int) getSizeSum(South_America)) + "MB");
            data.addAll(Misc);
            mAdapter.notifyDataSetChanged();
        }
    }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = actionMode.getMenuInflater();
            inflater.inflate(R.menu.menu_download, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_delete:
                    List<Integer> selectedItemPositions = mAdapter.getSelectedItems();
                    int currPos;
                    for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
                        currPos = selectedItemPositions.get(i);
                    }
                    actionMode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            this.actionMode = null;
            mAdapter.clearSelections();
        }

    public class mapData{

        String url = "";
        String name = "";
        String size = "";
        String date = "";
        boolean expanded = false;

        void setUrl(String a){
            url = a;
        }

        void setName(String a){
            name = a;
        }

        void setSize(String a){
            size = a;
        }

        void setDate(String a){
            date = a;
        }

        String getUrl(){
            return url;
        }

        String getName(){
            return name;
        }

        String getSize(){
            return size;
        }

        String getDate(){
            return date;
        }

        void toggleExpand() {expanded = !expanded;}

        boolean isExpanded() {return expanded;}
    }

}
