package mytracks;

import android.content.Context;

import org.mapsforge.map.android.view.MapView;

/**
 * Created by Alexander on 4/14/2015.
 */

// override the onTouchEvent
//http://mapsforge.googlecode.com/git-history/0.3.0/javadoc/org/mapsforge/android/maps/MapView.html
public class MytracksMapView extends MapView {
    private boolean centerMap = false;

    public MytracksMapView(Context context) {
        super(context);
    }


    @Override
    public boolean	onTouchEvent(android.view.MotionEvent motionEvent){

        setCenterMap(false);


        return super.onTouchEvent(motionEvent);
    }

    boolean isCenterMap(){
        return centerMap;
    }

    void setCenterMap(boolean isCenter){
        centerMap = isCenter;
    }
}
