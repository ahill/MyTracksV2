package mytracks;

/**
 * Created by Alexander on 3/18/2015.
 */
public class Point {
    double Lat;     //
    double Lon;     //
    float Speed;    // mph
    int Direction;  // 0 - 360 degrees

    public Point(double lat,double lon){
        Lat = lat;
        Lon = lon;

    }

    public Point(double lat,double lon,float speed){
        Lat = lat;
        Lon = lon;
        Speed = speed;
    }

}
