package mytracks;
/**
 * Created by ahill on 9/20/14.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;


public class customDraw extends View {
    Paint paint = null;
    public int offset = 0;

    boolean isFullScreen = false; // used to toggle between fullscreen etc.

    /////////////////////////////////
    // GPS info fields
    // these fields are updated by the main activity every half a second.
    double GPSInfoLat = 0;         //
    double GPSInfoLong = 0;        //
    double GPSInfoSpeed = 0;       // MPH
    double GPSInfoAccuracy = -1;
    long   GPSInfoTime = 0;
    long GPSInfoUpdates = 0;
    String Address = new String();
    public customDraw(Context context,  AttributeSet atri) {
        super(context,atri);

        paint = new Paint();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getContext());

        float maxX = canvas.getWidth();
        float maxY = canvas.getHeight();
        float cntX = canvas.getWidth() / 2;
        float cntY = canvas.getHeight() /2;

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        canvas.drawPaint(paint);

        paint.setColor(Color.GREEN);
        paint.setARGB(50,100,100,100);

        canvas.drawLine(0,cntY, maxX,cntY, paint);

        paint.setTextSize(50);
        paint.setStrokeWidth(10);
        paint.setColor(Color.BLACK);
        float LineSpace = 40;
        float TabAmount = 40;
        float yTop = 100;
        float xTop = 100;
        canvas.drawText("Your Location:",1*TabAmount+xTop,1*LineSpace + yTop,paint);
        canvas.drawText(String.format("Lat: %f",GPSInfoLat),2*TabAmount+xTop,2*LineSpace+yTop,paint);
        canvas.drawText(String.format("Lon: %f",GPSInfoLong),2*TabAmount+xTop,3*LineSpace+yTop,paint);
        canvas.drawText(String.format("Accuracy: %f",GPSInfoAccuracy),2*TabAmount+xTop,4*LineSpace+yTop,paint);
        canvas.drawText(String.format("Updates: %d",GPSInfoUpdates),2*TabAmount+xTop,5*LineSpace+yTop,paint);
        canvas.drawText("Address: " + Address,1*TabAmount+xTop,6*LineSpace+yTop,paint);

        this.invalidate();
        canvas.restore();
    }

    public void UpdateAddress(String s){
        Address = s;

    }

    public void UpdateGPSInfo(double Latitude,double Longitude,double Speed){
        this.GPSInfoLat = Latitude;
        this.GPSInfoLong = Longitude;
        this.GPSInfoSpeed = Speed;
        //System.out.println("Coverting back: " + Latitude + " =>  " + PointToCordsY(CordsToViewX((float)Longitude)) + " " + PointToCordsY(CordsToViewX((float)Longitude)+1));
    }

    public void UpdateGPSInfoExtras(double Accuracy,long time,long GPSUpdates){
        this.GPSInfoAccuracy = Accuracy;
        this.GPSInfoTime = time;
        this.GPSInfoUpdates = GPSUpdates;
    }
}
